import urllib.request
import urllib.error
import json
import os
import platform
import ipaddress

route = []
display = False
GEO_PATH = {}
lat = []
long = []
PATH = {}


def clear_data():
  global route
  global GEO_PATH
  global PATH
  global lat
  global long
  lat = []
  long = []
  GEO_PATH = {}
  route = []
  PATH = {}
  if display == True:
    print('route' + str(route))
    print('lat' + str(lat))
    print('long' + str(long))
    print('GEO_PATH' + str(GEO_PATH))
    print('PATH' + str(PATH))


def get_info(target, n):
  GEO_DATA = {}
  tla = ""
  tlo = ""
  with urllib.request.urlopen("http://ip-api.com/json/{}?fields=country,regionName,city,lat,lon".format(target), timeout=8) as url:
      data = url.read().decode()
      #data = data.split("(")[1].strip(")")
      loc_data = json.loads(data)
      if loc_data['country'] == None:
        if display == True:
          print('no load')
        pass
      else:
        if display == True:
          print(n)
        GEO_DATA['hop{}'.format(n)] = n
        GEO_DATA['target{}'.format(n)] = target
        GEO_DATA['Country{}'.format(n)] = loc_data['country']
        GEO_DATA['City{}'.format(n)] = loc_data['city']
        GEO_DATA['State{}'.format(n)] = loc_data['regionName']
        tla = loc_data['lat']
        tlo = loc_data['lon']
        if display == True:
          print(loc_data)
          print(GEO_DATA)
          print(lat)
          print(long)
        lat.append(tla)
        long.append(tlo)
  return GEO_DATA

def OS_trace(HOST):
  if platform.system() == 'Windows':
    HOST_list = os.popen('tracert -d -w 2000 -4 {}'.format(HOST)).read()
  else:
    HOST_list = os.popen('traceroute -n {}'.format(HOST)).read()
  return HOST_list

def validate(HOST_list):
  locations = HOST_list.split(' ')
  for l in locations:
    try:
      if ipaddress.ip_address(l):
        if ipaddress.ip_address(l).is_private:
          pass 
        else:
          route.append(l)
    except ValueError as er:
      pass 
  return route

def get_latency(HOST_list):
  time = {}
  jumble = HOST_list.split('\n')
  for i in jumble:
    tp = i.split(' ')
    ftp = ' '.join(tp).split()
    ftp = [ftp.replace('<1', '0.7') for ftp in ftp]
    try:
      v = ipaddress.ip_address(ftp[-1])
    except:
      if 'v' in locals():
        del v
    if 'v' in locals():
      try:
        avg = (float(ftp[1]) + float(ftp[3]) + float(ftp[5]))/3
        time[str(v)] = avg
      except:
        if display == True:
          print('bad hop')

  return time

def cycle(route):
  n = 0
  for stop in route:
    try:
      PATH.update(get_info(stop, n))
    except urllib.error.URLError as er:
      try:
        PATH.update(get_info(stop, n))
      except urllib.error.URLError as er:
        try:
          PATH.update(get_info(stop, n))
        except urllib.error.URLError as er:
          pass
    n+=1
  path_hops = [PATH, n]          
  return path_hops

def main_trace(HOST):
  clear_data()
  HOST_list = OS_trace(HOST)
  latency = get_latency(HOST_list)
  route = validate(HOST_list)

  if route[0] == HOST:
    route.pop(0)
  elif route[-1] != HOST:
    route.append(HOST)  
  

  if display == True:
    print('printing route')
    print("route" + str(route))
  
  tmp = cycle(route)
  number_hops = tmp[1]
  if isinstance(tmp[0], dict):
    GEO_PATH.update(tmp[0])
  else:
    pass
  
  data_return = [GEO_PATH, lat, long, latency, number_hops]
  if display == True:
    print('Trace complete')
  return data_return
  
if __name__ == '__main__':
  while True:
    HOST = input("target: ")
    clear_data()
    print(main_trace(HOST))

#EOF