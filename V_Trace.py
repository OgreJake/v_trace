#jakes GUI
from tkinter import *
from time import sleep
import socket
import geoIP
import basic
import thread
import images
import base64

#set debug display
display = False

#create data folder for images
WRK_DIR = geoIP.os.getenv('APPDATA')
WRK_DIR = WRK_DIR + '\\V_Trace\\'
if not geoIP.os.path.exists(WRK_DIR):
  geoIP.os.makedirs(WRK_DIR)

##  function definitions
def loading():
  #display loading GIF in window
  try:
    for i in range(0, 23):
      frame = photo2g[i]
      photo.configure(image=frame, background='white')
      photo.image = frame
      main_window.update()
      sleep(45/1000)
  except Exception as e:
    if display == True:
      print(e)

def close():
  main_window.destroy()
  #exit(0)

def start():
  photo.configure(image=photo2)
  photo.image = photo2
  lbo.delete(0.0, END) 
  lbo.insert(END, "Tracing Route now")
  if display == True:
    print('Starting Trace')
  main_window.update()
  trace()

def trace():
  TARGET = lbi.get()
  try:
    geoIP.ipaddress.ip_address(TARGET)
  except:
    if display == True:
      print('resolving host')
    try:
      TARGET = socket.gethostbyname(TARGET)
      if display == True:
        print("resolved to: " + str(TARGET))
      lbi.delete(0, END)
      lbi.insert(END, TARGET)
      main_window.update()
    except Exception as e:
      if display == True:
        print(e)
      lbo.delete(0.0, END)
      photo.configure(image=photo1)
      photo.image = photo1
      lbo.insert(END, "unable to resolve host")
      main_window.update()
      return
  try:
    if 'data' in locals() and display == True:
      print('old data ' + str(data))
    data_thread = thread.TraceThread(TARGET)
    data_thread.daemon = True
    data_thread.start()
    while data_thread.is_alive():
      loading()
    data = data_thread.return_data
    if display == True:
      print('data output')
      print(data)
    final_out = ""
    out = ""
    hops = []
    for i in range(data[4]):
      uni = 'IP: ' + data[0]['target' + str(i)] + '\n'
      uni = uni + 'Latency: {0:.2f}ms'.format(data[3][data[0]['target' + str(i)]]) + '\n'
      uni = uni + 'Country: ' + data[0]['Country' + str(i)] + '\n'
      uni = uni + 'State: ' + data[0]['State' + str(i)] + '\n'
      uni = uni + 'City: ' + data[0]['City' + str(i)]
      hops.append(uni)
      out = out + '\n' + '\n' + uni
    lbo.delete(0.0, END)
    final_out = out
  except Exception as e:
    out = "trace failed" + '\n' + str(e)
  if 'data' in locals():
    basic.make_map_from_route(data[1], data[2], hops, data[4])
  photo.configure(image=photo1)
  photo.image = photo1
  lbo.insert(END, final_out)
  lbi.delete(0, END)
  main_window.update()

#Create Images
icon_image = WRK_DIR + 'icon.ico'
ifhic = open(icon_image, 'wb')
ifhic.write(base64.decodebytes(images.icon_img))
ifhic.close
main_image = WRK_DIR + 'image.gif'
ifhi = open(main_image, 'wb')
ifhi.write(base64.b64decode(images.main_img))
ifhi.close
load_image = WRK_DIR + "loading.gif"
ifhl = open(load_image, "wb")
ifhl.write(base64.decodebytes(images.load_img))
ifhl.close
 
#Define main window
main_window = Tk()
main_window.iconbitmap(icon_image)
main_window.title(" Jakes Traceroute ")
main_window.configure(background='black')

#Setup main images and text boxes
photo1 = PhotoImage(file=main_image)
photo2 = PhotoImage(file=load_image)
photo2g = [PhotoImage(file=load_image, format = 'gif -index %i' %(i)) for i in range(24)]
photo = Label(main_window, image=photo1, bg='black')
photo.grid(column=0, row=0, columnspan=3)

lbi = Entry(main_window, font=('none', 40), width=20)
lbi.grid(column=0, row=1)
lbi.bind("<Return>", (lambda event: start()))

lbo = Text(main_window, height=20, width=40, wrap=WORD, background='white')
lbo.grid(column=2, row=1, sticky=W, rowspan=2)

#Setup buttons
btn = Button(main_window, text='Trace', font=('none', 25), width=5, command=start)
btn.grid(column=0, row=2, sticky=N)

btn_exit = Button(main_window, text='Exit', font=('none', 25), width=5, command=close)
btn_exit.grid(column=0, row=2, sticky=S)

#run main window
main_window.mainloop()

#EOF