
mapbox_access_token = 'pk.eyJ1IjoiYmlnb2dyZTU1IiwiYSI6ImNqcmt1a3cycjAxcng0NWw5amU2bXA1OHYifQ.phl2AtS_L3ucqmRBGFBuMg'

import plotly.plotly as pl
import plotly.offline as opl
import plotly.graph_objs as ga

red = 'rgb(255, 10, 10)'
yellow = 'rgb(150, 150, 20)'
green = 'rgb(10, 255, 10)'
blue = 'rgb(20, 15, 250)'

def create_layout(la, lo):
  layout = ga.Layout(
    title = 'Your Route',
    autosize=True,
    showlegend = False,
    hovermode='closest',
	geo = dict (
	  resolution = 50,
	  scope='north america',
	  projection=dict( type='natural earth' ),
	  showland = True,
	  showlakes = True,
	),
    mapbox=dict(
      accesstoken=mapbox_access_token,
      bearing=1,
      center=dict(
        lat=la,
        lon=lo
      ),
      pitch=10,
      zoom=1
    ), 
  )
  return layout

def make_lines(lat, long, hops):
  lines = [ dict(
    type = 'scattergeo',
    lon = long,
    lat = lat,
    mode = 'text',
    hoverinfo = 'skip',
    line = dict(
      width = 2,
      color = 'rgb(20, 80, 255)',
    ),
    opacity = 0.8,
    text = hops,
    showlegend = False
  )]
  
  return lines

def set_marker_color(t):
  n = float(t)
  if n > 30:
    color = red
  elif n < 30 and n > 15:
    color = yellow
  elif n < 15 and n > 0:
    color = green
  
  return color

def make_markers(lat, long, hops):
  latency = []
  colors = []
  for s in hops:
    rtt = s.split('\n')
    tim = rtt[1].split(': ')
    latency.append(tim[1][0:-2])
  for time in latency:
    colors.append(set_marker_color(time))
  markers = [ dict(
    type = 'scattergeo',
    lon = long,
    lat = lat,
    hoverinfo = 'text',
    mode = 'markers+lines',
    marker = dict(
      size = 8,
      color = blue, #set_marker_color(t),
      line = dict(
        width = 5,
        color = colors
      )
    ),
    text = hops
  )]
  return markers

def make_route(lat, long, hops, nhops):
  markers = make_markers(lat, long, hops)
  inc_hop = []
  for i in range(nhops):
    inc_hop.append(i)
  lines = make_lines(lat, long, inc_hop)
  data = markers #+ lines 
  return data

def build_map(fig):
  #pl.plot(data, filename = 'basic-line', auto_open=True) for online plots that save
  opl.plot(fig, filename = 'my_route.html', auto_open=True)
  
def make_map_from_route(lat, long, hops, nhops):
  layout = create_layout(lat[1], long[1])
  data = make_route(lat, long, hops, nhops)
  fig = dict(data=data, layout=layout)
  build_map(fig)
  
if __name__ == '__main__':
  
  lat = [35.8, 31.3, 36.6]
  long = [-85.6, -96.8, -119.4]
  t = 40
  hops = ["A", "B", "C"]
  layout = create_layout(lat[1], long[1])
  data = make_route(lat, long, hops)
  fig = dict(data=data, layout=layout)
  build_map(fig)
  